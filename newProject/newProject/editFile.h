#ifndef __EDITFILE_H__
#define __EDITFILE_H__

int rotatePicture(PICTURES**);
int** allocateMatrix(int, int);
void printPicture(PICTURES**);

#endif

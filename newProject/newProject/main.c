#ifndef __MAIN_C__
#define __MAIN_C__

#include "main.h"
#include "menu.h"
#include "diskOperations.h"
#include "editFile.h"

int main()
{
    int nrOfpictures = 0;
    int menuButton = 0;
    PICTURES *pictTABLE = NULL, *ptrACTIVE = NULL;

    for( ; ; )
    {
        system("cls");
        displayMenu(&nrOfpictures, &pictTABLE, &ptrACTIVE);
        menuButton = _getch();
        system("cls");

        switch (menuButton) {
        case '1':
            /* createStruct struct[0] */
            if (nrOfpictures == 0)
            {
                if(createStruct(&nrOfpictures, &pictTABLE) == 1)
                /* fatal error */
                {
                    return 0;
                };
            }
            else
            /* Realloc struct[n] for one image more */
            {
                if(enlargeStruct(&nrOfpictures, &pictTABLE, &ptrACTIVE) == 1 )
                /* fatal error */
                {
                    return 0;
                };
            }
            break;
        case '2':
            removeStruct(&nrOfpictures, &pictTABLE, &ptrACTIVE);
            break;
        case '3':
            makeActive(&nrOfpictures, &pictTABLE, &ptrACTIVE);
            break;
        case '4':
            printPicture(&ptrACTIVE);
            break;
        case '5':
            rotatePicture(&ptrACTIVE);
            break;
        case '6':
            saveToFile(&ptrACTIVE);
            break;
        case 'X':
        case 'x':
            exitProgram(&nrOfpictures, &pictTABLE);
            pictTABLE = NULL;
            ptrACTIVE = NULL;
            return 0;
        default:
            printf("Nothing was choosen \n");
            break;
        }
    }
    return 0;
}

#endif

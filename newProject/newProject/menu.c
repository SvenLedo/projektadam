#ifndef __MENU_C__
#define __MENU_C__

#include "main.h"
#include "menu.h"
#include "diskOperations.h"
#include "editFile.h"

void displayMenu(const int* counter, PICTURES** dispTABLE, PICTURES** dispACTIVE)
{
    char picName[30];

    if(*dispACTIVE == NULL)
    {
        strcpy(picName, "--PICTURE NOT SELECTED--");
    }
    else
    {
        strcpy(picName, (*dispACTIVE)->fileName);
    }

    printf("---------------------------------------------------------------------\n");
    printf("Please choose correct character:\t Loaded pictures:%d\n", *counter);
    printf("[1] Add    picture to database \n");
    printf("[2] Remove picture from database \n");
    printf("[3] Choose picture to edit: \n");
    printf("\n");
    printf("Edit active picture: %s \n", picName);
    printf("[4] Print picture to the screen \n");
    printf("[5] Rotate 90 deg \n");
    printf("[6] Save active picture as a new file \n");
    printf("\n");
    printf("[x] for exit\n");
    printf("---------------------------------------------------------------------\n");
}

void makeActive(const int* counter, PICTURES** makeTABLE, PICTURES** makeACTIVE)
{
    int bin = 0;
    if(*counter == 0)
    {
        printf("Database is empty \n");
        printf("Add picture from the main menu first \n\n");
        system("pause");
    }
    else
    {
        /* Printing list of added images */
        printf("Please choose correct number and press enter \n\n");
        for(int i = 0; i < *counter; i++)
        {
            printf("nr:%d\t%s \n", i+1, (*makeTABLE+i)->fileName);
        }

        printf("\nType nr: \n");
        scanf("%d", &bin);
        for(int i = 0; i < *counter; i++)
        {
            if(bin-1 == i)
                *makeACTIVE = (*makeTABLE+i);
        }
        printf("Activating: %s \n", (*makeACTIVE)->fileName);
    }
}

int createStruct(int* counter, PICTURES** createTABLE)
{
    /* Allocating memory for "structure[0]" */
    getMemory(counter, &createTABLE);

    /* 1st IF  - exception for memory allocation, before file being loaded */
    if(*createTABLE == NULL)
    {
        printf("Load failed, please try again \n\n");
        system("pause");
        return 0;
    }
    else
    {
        int errCode = 0;
        errCode = loadFromFile(&createTABLE);

        /* 2nd IF - exception for error during loading file */
        if(errCode == 2)
        {
            printf("Load failed, please try again \n");
            free( *createTABLE );
            *createTABLE = NULL;
            system("pause");
            return 0;
        }
        else if(errCode == 1)
        {
            /* succeed, (*counter)++ ->picture added to the database */
            (*counter)++;
            return 0;
        }
        else
        {
            /* MISRA standard */
            return 0;
        }
    }
}

int enlargeStruct(int* counter, PICTURES** enlargeTABLE, PICTURES** enlargeACTIVE)
{
    /* Reallocating memory for Structure */
    getMemory(counter, &enlargeTABLE);

    int countLocal = *counter;
    int errCode = 1;
    if(*enlargeTABLE == NULL)
    {
        printf("Critical error, terminating program \n");
        system("pause");
        return 1;
    }
    else
        /* Moving pointer from starting address to the newly created structure */
    {
        (*enlargeTABLE) += countLocal;
        errCode = loadFromFile(&enlargeTABLE);
       // printf("counter: %d", *counter);

        /* errCode == 2, picture load fail, reallocating struct to previous shape */
        if(errCode == 2)
        {
            printf("Load failed, please try again \n");
            /* Moving pointer from newly created structure back to starting address, and downgrading memory */
            (*enlargeTABLE) -= countLocal;
            *enlargeTABLE = (PICTURES*)realloc(*enlargeTABLE, (*counter)*sizeof(PICTURES));
            if(*enlargeTABLE == NULL)
            {
                printf("Critical error, terminating program \n");
                return 1;
            }
            system("pause");
            return 0;
        }
        /* errCode == 1, picture has been loaded successfully*/
        else if(errCode == 1)
        {
            (*counter)++;

            /* Replacing enlargeACTIVE from old address (not valid any more) to relocated address */
            if(enlargeACTIVE != NULL)
            {
                *enlargeACTIVE = *enlargeTABLE;
            }
        }

        /* Moving back pointer to the starting address */
        (*enlargeTABLE) -= countLocal;

        /* Replacing enlargeACTIVE from old address (not valid any more) to relocated address */
        if(enlargeACTIVE != NULL)
        {
            *enlargeACTIVE = *enlargeTABLE;
        }
        return 0;
    }
}

void getMemory(const int* counter, PICTURES*** getMemTABLE)
{
    if(*counter == 0)
    {
        **getMemTABLE = (PICTURES*)malloc(sizeof(PICTURES));
    }
    else
    {
        **getMemTABLE =(PICTURES*)realloc(**getMemTABLE, ((*counter)+1)*sizeof(PICTURES));
    }
}

void removeStruct(int* counter, PICTURES** remTABLE, PICTURES** remACTIVE)
{

    int bin = 0;
    if(*counter == 0)
    {
        printf("Database is empty \n");
        printf("Nothing to remove \n\n");
        system("pause");
    }
    else
    {
        printf("Please choose correct number of picture to be removed from database \n\n");

        /* Printing list of added images */
        for(int i = 0; i < *counter; i++)
        {
            printf("nr:%d\t%s \n", i+1, (*remTABLE+i)->fileName);
        }

        /* Asking to pick a file */
        printf("\nType nr: \n");
        scanf("%d", &bin);

        /* if statment under is true, it means that only one image is loaded */
        if((bin == 1) && (*counter == 1))
        {
            (*counter)--;
            deallocateMatrix(&remTABLE);
            free(*remTABLE);
            *remTABLE = NULL;
            *remACTIVE = NULL;
        }
        else
            /* or false, it means there are many pictures, and structure needs sorting */
        {
            --bin;
            for(int i = 0; i < *counter; i++)
            {
                if( bin == i )
                {
                    /* example:
                    User wants to delete second picture (bin=2) from the array
                    bin==2 is equivalent to second index i==1,-> remTABLE[1].
                    --bin == 1 gives true, assigning ptr to remTABLE[0+1] */

                    *remACTIVE=(*remTABLE+bin);
                    break;
                }
            }
            /* ptr points on struct to be removed, to clear dynamic multi dim array */
            deallocateMatrix(&remACTIVE);


            /* setting ptr on struct[1+1] - index greater, to begin replacement of indices - sorting */
            *remACTIVE=(*remTABLE+bin+1);

            /* amount of left indices to sort */
            int reduceBY=(*counter-1)-bin;

            /* replacing struct[2] to struct [1], struct[3] to struct[2] , struct[n] to struct[n-1] ... - sorting */
            for(int i = 0; i < reduceBY; i++)
            {
                memcpy((*remACTIVE-1),(*remACTIVE),sizeof(PICTURES));
                (*remACTIVE)++;
            }

            /*  after sorting, downgrading size of container, removing last "empty" structure */
            (*counter)--;
            *remTABLE = realloc(*remTABLE,(*counter)*sizeof(PICTURES));
            *remACTIVE = NULL;
        }
    }
}

/* Function to deallocate field of structure - multi dimension matrix int** Table; */
void deallocateMatrix(PICTURES*** deallocTABLE)
{
    for(int i = 0; i < (**deallocTABLE)->Rows; i++)
    {
        free((**deallocTABLE)->Table[i]);
    }
    free((**deallocTABLE)->Table);
}

void exitProgram(const int* counter, PICTURES **exitTABLE)
{
    if(*exitTABLE != NULL)
    {
        /* Deallocating each structure field -> int** Tab */
        for(int i = 0; i < *counter; i++)
        {
            deallocateMatrix(&exitTABLE);
            (*exitTABLE)++;
        }
        (*exitTABLE)-= *counter;
        free(*exitTABLE);
    }

    printf("Project has been created by Adam Smalira. \n");
    printf("adam.smalira@mail.com \n\n");
    printf("Thank you for using PGM-Editor \n\n");
}

#endif

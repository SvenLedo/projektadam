#ifndef __MENU_H__
#define __MENU_H__

void displayMenu(const int*, PICTURES**, PICTURES**);
void makeActive(const int*, PICTURES**, PICTURES**);
int createStruct(int*, PICTURES**);
int enlargeStruct(int*, PICTURES**, PICTURES**);
void getMemory(const int*, PICTURES***);
void removeStruct(int*, PICTURES**, PICTURES**);
void deallocateMatrix(PICTURES***);
void exitProgram(const int*, PICTURES**);

#endif
